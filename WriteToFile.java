package filewriting;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * 
 * @author AnishKrishnan
 * 
 * This program allows users to append to a file. If the specified file does not exist,
 * it will be created. While this program allows for direct user input, please not that
 * it can be used as a foundation for logging for larger programs.
 *
 */
public class WriteToFile {

	public static void main(String[] args) {
		String fd = "C:/Users/Anish/Desktop/newfile.txt";
		File file;
		FileWriter fw;
		BufferedWriter bw;
		Scanner in;

		try {
			file = new File(fd);
			
			/* If the file does not already exist, create it. */
			if (!file.exists()) {
				System.out.println("Creating new file...");
				file.createNewFile();
				System.out.println("Done.\n");
			}
			
			/* In the instantiation of the FileWriter object, we specify that we
			 * want to append to the file rather than overwrite the existing text. */
			fw = new FileWriter(file, true);
			bw = new BufferedWriter(fw);
			in = new Scanner(System.in);
			
			System.out.println("Type lines to be written to the file and hit [Enter].");
			System.out.println("Type /quit to end the writing process.\n");
			String line;
			
			/* Loop indefinitely until the user types "/quit." */
			while (true) {
				line = in.nextLine();
				if (line.equals("/quit"))
					break;
				bw.append(line + "\n");
			}
			
			/* Flush and close the writers. */
			bw.flush();
			bw.close();
			fw.close();
			
		} catch (IOException e) {
			System.out.printf("ERROR: Cannot create file at directory %s", fd);
			e.printStackTrace();
		}
	}
}
